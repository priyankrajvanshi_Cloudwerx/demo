/**
 * @description  This selector class is used to fetch data of Account object.
 * @author  Priyank
**/
public inherited sharing class AccountSelector {
    /**
    * @description This method is used to get active account list
    * @author Priyank 
    * @return this method will retrn List of Active Accounts
    **/
    public static List<Account> getActiveAccount() {
        return[
            SELECT Id, Active__c, Name
            FROM Account
            WHERE Active__c = 'Yes'
        ];
    }
}
