/**
 * @description  THis class is used to handle the logic of Account object.
 * @author  Priyank
**/
public with sharing class AccountDomain {
    
    /**
    * @description 
    * @author ChangeMeIn@UserSettingsUnder.SFDoc | 10-19-2021 
    * @return Boolean 
    **/
    public Boolean isActiveAccountExist(){
        Boolean isActive;
        for(Account a : AccountSelector.getActiveAccount()) {
            if(logic) {
                isActive = true;
            }
        }
        return isActive;
    }
}
