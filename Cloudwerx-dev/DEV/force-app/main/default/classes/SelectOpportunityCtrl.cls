/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-19-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public with sharing class SelectOpportunityCtrl {

    /**
    * @description 
    * @author ChangeMeIn@UserSettingsUnder.SFDoc | 10-19-2021 
    * @return List<Account> 
    **/
    public List<Account> getActiveAccount() {
        AccountSelector acc = new AccountSelector();
        as.getActiveAccounts();
    }
}
